all: build firefox chrome

init-submodule:
	@if git submodule status | egrep -q '^[-]|^[+]' ; then \
		echo "INFO: Need to reinitialize git submodules"; \
		git submodule update --init; \
	else \
		echo "INFO: Submodules up to date"; \
	fi

build:
	./bin/build_plugin

firefox: build
	./bin/package_plugin firefox

chrome: build
	./bin/package_plugin chrome

clean:
	if test -d build; then rm -rf build; fi

ui-update:
	cd ui && git pull && cd ..
