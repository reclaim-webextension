// Default settings. If there is nothing in storage, use these values.
const defaultSettings = {
    reclaim_alive: false,
};

chrome.storage.local.set(defaultSettings);

const authzSettings = {
    search: null,
    request: false
};

chrome.storage.local.set(authzSettings, function() {});

function handle_onbefore_request(request)
{
    console.log(request)
    var url = new URL(request.url);

    if(url.hostname == "ui.reclaim") {
        authzSettings.search = request.url.split("?")[1];
        authzSettings.request = true;
        chrome.storage.local.set(authzSettings, function() {});
        console.log(authzSettings);
        //browser.runtime.openOptionsPage();
        //return {};
        return {"redirectUrl": chrome.runtime.getURL("/index.html") + "?" + request.url.split("?")[1]}
    }
    if (url.hostname == "api.reclaim") {
        return {"redirectUrl": chrome.runtime.getURL("/index.html") + "?authz_request=true" + "&pathname=" + url.pathname + "&" + request.url.split("?")[1] + "&search=" + url.search};
    }

}

chrome.webRequest.onBeforeRequest.addListener(handle_onbefore_request,
    {
        urls: ["*://*.reclaim/*"],
        //types: ["beacon", "csp_report", "font", "image", "imageset", "main_frame", "media", "object", "ping", "speculative", "stylesheet", "sub_frame", "web_manifest", "websocket", "xbl", "xml_dtd", "xslt", "other", "script", "xmlhttprequest"]
        types: ["csp_report", "font", "image", "main_frame", "media", "object", "ping", "stylesheet", "sub_frame", "websocket", "other", "script", "xmlhttprequest"]

    },
    ["blocking"]
);

