const reclaim_link = document.querySelector("#reclaim-link");
const reclaim_logo = document.querySelector("#reclaim-logo");
const reclaim_dead = document.querySelector("#reclaim-dead");
const reclaim_alive = document.querySelector("#reclaim-alive");

// Update the options UI with the settings values retrieved from storage,
// or the default settings if the stored settings are empty.
function updateUI(restoredSettings) {
  if (restoredSettings.reclaim_alive) {
    reclaim_link.style.display = 'inline-block';
    reclaim_alive.style.display = 'inline-block';
    reclaim_dead.style.display = 'none';
    reclaim_logo.href = "../index.html";
  } else {
    reclaim_logo.removeAttribute("href");
    reclaim_dead.style.display = 'inline-block';
    reclaim_alive.style.display = 'none';
    reclaim_link.style.display = 'none';
  }
  //reclaim_active.checked = restoredSettings.reclaim_alive;
}

function onError(e) {
  console.error(e);
}

// On opening the options page, fetch stored settings and update the UI with them.
chrome.storage.local.get(['reclaim_alive'], function(result) {
  updateUI(result);
});

// Whenever the contents of the textarea changes, save the new values
//reclaim_active.addEventListener("change", storeSettings);
//reclaim_active.addEventListener("change", storeSettings);

