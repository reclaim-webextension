function check_reclaim() {

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open('GET', 'http://localhost:7776/identity/all', true);

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == XMLHttpRequest.DONE) {
      var resp = xmlHttp.status;

      if (200 != resp) {
        var reclaim_alive = false;
        chrome.storage.local.set({reclaim_alive}, function() {});
        console.log("reclaim down (ret=" + resp + ")");
      } else {
        var reclaim_alive = true;
        chrome.storage.local.set({reclaim_alive}, function() {});
        console.log("reclaim up");
      }
    }
  }
  try {
    xmlHttp.send(null);
  } catch(e) {
    console.log(e);
  }

}

check_reclaim();
var intervalId = setInterval(check_reclaim, 5000);

